import java.util.HashMap;
import java.util.Scanner;


public class App {
    public static void main(String[] args) throws Exception {
        Customer customer;
        String email, productName;
        int quantity;
        HashMap<String, Customer> customers = new HashMap<>(); // stores list of customer accounts
        // Hashmap used to optimize searching of customer.
        // maps user email to customer
        HashMap<String, Product> products = new HashMap<>(); // stores list of products
        // maps product name to product object
        
        Scanner sc = new Scanner(System.in);
        
        //Initialization
        addProducts(products); // provides a list of products in products
        addCustomers(customers); // provides a list of customer accounts
        
        
        // Customer sign in 
        System.out.print("Enter the customer email to sign in__ ");
        email = sc.nextLine();
        customer = customers.getOrDefault(email, null);
        if(customer==null){
            System.out.println("Invalid email");
        }
        else{
            do{
                System.out.println("Signed in as " + customer.getName());
                
                listProducts(products);
                
                System.out.print("Enter the Product Name you want to buy__ ");
                productName = sc.nextLine();
                
                System.out.print("Enter the No of products  you want to buy__ ");
                quantity = Integer.parseInt(sc.nextLine());
    
                Product productToBuy = products.getOrDefault(productName, null);
                if(productToBuy!=null && productToBuy.getQuantity()>=quantity){
                    productToBuy.setQuantity(productToBuy.getQuantity()-quantity);
                    customer.addToCart(productToBuy, quantity);
                    System.out.println("\nAfter adding to cart,");
                    System.out.println("For "+customer.getName()+", \n"+ customer.getCart());
                    listProducts(products);
                    
                }
                else{
                    System.out.println("Not enough products");
                }
                System.out.print("Do you wish to check Out? ");
            }while(sc.nextLine().equalsIgnoreCase("No"));
            System.out.println("Purchase Successful! Rs. "+ customer.getCart().getCartPrice()+" debited from your account. This purchase is non refundable. Any disputes regarding this are not subjected to any jurisdiction.");
            System.out.println("Thank you for shopping with us.");
        }

        sc.close();
    }
    
    
    public static void listProducts(HashMap<String, Product> products){
        System.out.println("==========================================================");
        System.out.println("The products available are:");
        System.out.println(products.values().toString());
        System.out.println("==========================================================");
    }

    public static void addProducts(HashMap<String, Product> products ){
        products.put("Product1", new Product("Product1", "Brand1", 10, 100));        
        products.put("Product2", new Product("Product2", "Brand2", 20, 200));        
        products.put("Product3", new Product("Product3", "Brand3", 30, 300));        
        products.put("Product4", new Product("Product4", "Brand4", 40, 400));        
    }

    public static void addCustomers(HashMap<String, Customer> customers){
        customers.put("email1", new Customer("Customer1", "email1", "123456789"));
        customers.put("email2", new Customer("Customer2", "email2", "123456789"));
        customers.put("email3", new Customer("Customer3", "email3", "123456789"));
        
    }


}
