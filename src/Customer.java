
public class Customer {
    private String name;
    private String email; // email address is used as ubique identifier for each customer
    private String phone;
    private Cart cart;

    public Customer(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        // static method that returns the cart linked to the customer email(for existing users)
        // if the customer does not have a cart, creates a new cart for the customer(for new users)
        //  and then returns it
        this.cart = Cart.getCart(email); 
    }

    public void addToCart(Product product, int quantity ) {
        cart.addToCart(product, quantity);
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public Cart getCart() {
        return cart;
    }
    public void setCart(Cart cart) {
        this.cart = cart;
    }

    


}
