public class Product {
    private String name;
    private String brand;
    private int quantity;
    private float price;
    
    public Product(String name, String brand, int quantity, float price) {
        this.name = name;
        this.brand = brand;
        this.quantity = quantity;
        this.price = price;
    }
    

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        String productDescription;
        productDescription = "---------------" + String.format("%s\n\t\tBrand = %s\n\t\tQty=%d\n\t\tMRP=%f\n", name, brand, quantity, price);
        return productDescription;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getBrand() {
        return brand;
    }


    public void setBrand(String brand) {
        this.brand = brand;
    }


    public int getQuantity() {
        return quantity;
    }


    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public float getPrice() {
        return price;
    }


    public void setPrice(float price) {
        this.price = price;
    }

    

}
