import java.util.HashMap;

public class Cart {
    private String id;
    private HashMap<String, Product> prodList; // List of products n the cart
    static HashMap<String, Cart> cartOf = new HashMap<>();
    
    private Cart(String email){ // Cart object can be created only from Cart class
        this.id = email;
        prodList = new HashMap<>();
    }

    public static Cart getCart(String email){
        if (cartOf.containsKey(email)){
            return cartOf.get(email);
        }
        cartOf.put(email, new Cart(email));
        return cartOf.get(email);
    }

    public boolean addToCart(Product product, int quantity){
        if (prodList.containsKey(product.getName())){
            prodList.get(product.getName()).setQuantity(quantity+prodList.get(product.getName()).getQuantity());
        }
        else{
            this.prodList.put(product.getName(), new Product(product.getName(), product.getBrand(), quantity, product.getPrice()));
        }
        return true;
    }

    public float getCartPrice(){
        float result = 0;
        for(Product product: prodList.values()){
            result += product.getPrice() * product.getQuantity();
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "The contents of the cart are:\n==============================================\n";
        for(Product product: prodList.values()){
            result += product.toString() + "\n";
        }
        result += "\n--------------Total = "+ getCartPrice();
        result += "\n================================================================";
        return result;
    }

}
