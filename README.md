# ASSIGNMENT 6

## App.java

Main class that contains the main function. 

```java
import java.util.HashMap;
import java.util.Scanner;


public class App {
    public static void main(String[] args) throws Exception {
        Customer customer;
        String email, productName;
        int quantity;
        
        
        HashMap<String, Customer> customers = new HashMap<>(); // stores list of customer accounts
        // Hashmap used to optimize searching of customer.
        // maps user email to customer
        

        HashMap<String, Product> products = new HashMap<>(); // stores list of products
        // maps product name to product object
        
        
        Scanner sc = new Scanner(System.in);
        
        //Initioalization
        addProducts(products); // provides a list of products in products
        addCustomers(customers); // provides a list of customer accounts
        
        
        // Customer sign in 
        System.out.print("Enter the customer email to sign in__ ");
        email = sc.nextLine();
        customer = customers.getOrDefault(email, null);
        if(customer==null){
            System.out.println("Invalid email");
        }
        else{
            do{
                System.out.println("Signed in as " + customer.getName());
                
                listProducts(products);
                
                System.out.print("Enter the Product Name you want to buy__ ");
                productName = sc.nextLine();
                
                System.out.print("Enter the No of products  you want to buy__ ");
                quantity = Integer.parseInt(sc.nextLine());
    
                Product productToBuy = products.getOrDefault(productName, null);
                if(productToBuy!=null && productToBuy.getQuantity()>=quantity){
                    productToBuy.setQuantity(productToBuy.getQuantity()-quantity);
                    customer.addToCart(productToBuy, quantity);
                    System.out.println("\nAfter adding to cart,");
                    System.out.println("For "+customer.getName()+", \n"+ customer.getCart());
                    listProducts(products);
                    
                }
                else{
                    System.out.println("Not enough products");
                }
                System.out.print("Do you wish to check Out? ");
            }while(sc.nextLine().equalsIgnoreCase("No"));
            System.out.println("Purchase Successful! Rs. "+ customer.getCart().getCartPrice()+" debited from your account. This purchase is non refundable. Any disputes regarding this are not subjected to any jurisdiction.");
            System.out.println("Thank you for shopping with us.");
        }

        sc.close();
    }
    
    
    public static void listProducts(HashMap<String, Product> products){
        System.out.println("==========================================================");
        System.out.println("The products available are:");
        System.out.println(products.values().toString());
        System.out.println("==========================================================");
    }

    public static void addProducts(HashMap<String, Product> products ){
        products.put("Product1", new Product("Product1", "Brand1", 10, 100));        
        products.put("Product2", new Product("Product2", "Brand2", 20, 200));        
        products.put("Product3", new Product("Product3", "Brand3", 30, 300));        
        products.put("Product4", new Product("Product4", "Brand4", 40, 400));        
    }

    public static void addCustomers(HashMap<String, Customer> customers){
        customers.put("email1", new Customer("Customer1", "email1", "123456789"));
        customers.put("email2", new Customer("Customer2", "email2", "123456789"));
        customers.put("email3", new Customer("Customer3", "email3", "123456789"));
        
    }


}
```

## Customer.java

To create objects of customer class

```java

public class Customer {
    private String name;
    private String email; // email address is used as ubique identifier for each customer
    private String phone;
    private Cart cart;

    public Customer(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        // static method that returns the cart linked to the customer email(for existing users)
        // if the customer does not have a cart, creates a new cart for the customer(for new users)
        //  and then returns it
        this.cart = Cart.getCart(email); 
    }

    public void addToCart(Product product, int quantity ) {
        cart.addToCart(product, quantity);
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public Cart getCart() {
        return cart;
    }
    public void setCart(Cart cart) {
        this.cart = cart;
    }

}

```

## Product.java

```java
public class Product {
    private String name;
    private String brand;
    private int quantity;
    private float price;
    
    public Product(String name, String brand, int quantity, float price) {
        this.name = name;
        this.brand = brand;
        this.quantity = quantity;
        this.price = price;
    }
    

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        String productDescription;
        productDescription = "---------------" + String.format("%s\n\t\tBrand = %s\n\t\tQty=%d\n\t\tMRP=%f\n", name, brand, quantity, price);
        return productDescription;
    }


    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getBrand() {
        return brand;
    }


    public void setBrand(String brand) {
        this.brand = brand;
    }


    public int getQuantity() {
        return quantity;
    }


    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public float getPrice() {
        return price;
    }


    public void setPrice(float price) {
        this.price = price;
    }

    

}
```

## Cart.java

```java
import java.util.HashMap;

public class Cart {
    private String id;
    private HashMap<String, Product> prodList; // List of products n the cart
    static HashMap<String, Cart> cartOf = new HashMap<>();
    
    private Cart(String email){ // Cart object can be created only from Cart class
        this.id = email;
        prodList = new HashMap<>();
    }

    public static Cart getCart(String email){
        if (cartOf.containsKey(email)){
            return cartOf.get(email);
        }
        cartOf.put(email, new Cart(email));
        return cartOf.get(email);
    }

    public boolean addToCart(Product product, int quantity){
        if (prodList.containsKey(product.getName())){
            prodList.get(product.getName()).setQuantity(quantity+prodList.get(product.getName()).getQuantity());
        }
        else{
            this.prodList.put(product.getName(), new Product(product.getName(), product.getBrand(), quantity, product.getPrice()));
        }
        return true;
    }

    public float getCartPrice(){
        float result = 0;
        for(Product product: prodList.values()){
            result += product.getPrice() * product.getQuantity();
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "The contents of the cart are:\n==============================================\n";
        for(Product product: prodList.values()){
            result += product.toString() + "\n";
        }
        result += "\n--------------Total = "+ getCartPrice();
        result += "\n================================================================";
        return result;
    }

}
```

## OUTPUT

```
Windows PowerShell

Try the new cross-platform PowerShell https://aka.ms/pscore6

PS C:\Users\SHREYANSHS\antwalkAssignments\assignment6>  & 'C:\Program Files\Java\jdk-11\bin\java.exe' '-cp' 'C:\Users\SHREYANSHS\antwalkAssignments\assignment6\bin' 'App'


Enter the customer email to sign in__ email3        <--------------- Email used to sign in
Signed in as Customer3                              <--------------- Sign In Successful
==========================================================
The products available are:                         <--------------- Product List
[---------------Product3
                Brand = Brand3
                Qty=30
                MRP=300.000000
, ---------------Product4
                Brand = Brand4
                Qty=40
                MRP=400.000000
, ---------------Product1
                Brand = Brand1
                Qty=10
                MRP=100.000000
, ---------------Product2
                Brand = Brand2
                Qty=20
                MRP=200.000000
]
==========================================================
Enter the Product Name you want to buy__ Product3      <----------- User Input
Enter the No of products  you want to buy__ 12         <----------- User Input

After adding to cart,
For Customer3,
The contents of the cart are:                          <----------- Cart Summary 
==============================================
---------------Product3
                Brand = Brand3
                Qty=12
                MRP=300.000000


--------------Total = 3600.0
================================================================
==========================================================      
The products available are:                <------------ Product List updated
[---------------Product3
                Brand = Brand3
                Qty=18
                MRP=300.000000
, ---------------Product4
                Brand = Brand4
                Qty=40
                MRP=400.000000
, ---------------Product1
                Brand = Brand1
                Qty=10
                MRP=100.000000
, ---------------Product2
                Brand = Brand2
                Qty=20
                MRP=200.000000
]
==========================================================      
Do you wish to check Out? No
Signed in as Customer3
==========================================================
The products available are:
[---------------Product3
                Brand = Brand3
                Qty=18
                MRP=300.000000
, ---------------Product4
                Brand = Brand4
                Qty=40
                MRP=400.000000
, ---------------Product1
                Brand = Brand1
                Qty=10
                MRP=100.000000
, ---------------Product2
                Brand = Brand2
                Qty=20
                MRP=200.000000
]
==========================================================
Enter the Product Name you want to buy__ Product1
Enter the No of products  you want to buy__ 50
Not enough products                    <------------- Enough Products not available
Do you wish to check Out? No
Signed in as Customer3
==========================================================
The products available are:
[---------------Product3
                Brand = Brand3
                Qty=18
                MRP=300.000000
, ---------------Product4
                Brand = Brand4
                Qty=40
                MRP=400.000000
, ---------------Product1
                Brand = Brand1
                Qty=10
                MRP=100.000000
, ---------------Product2
                Brand = Brand2
                Qty=20
                MRP=200.000000
]
==========================================================
Enter the Product Name you want to buy__ Product2
Enter the No of products  you want to buy__ 5

After adding to cart,
For Customer3,
The contents of the cart are:
==============================================      ____
---------------Product3                                 |
                Brand = Brand3                          |
                Qty=12                                  |Cart
                MRP=300.000000                          |Summary
                                                        |
---------------Product2                                 |
                Brand = Brand2                          |
                Qty=5                                   |
                MRP=200.000000  |                       |    
                                                        |
                                                    ____|
--------------Total = 4600.0                            
================================================================
==========================================================
The products available are:
[---------------Product3
                Brand = Brand3
                Qty=18
                MRP=300.000000
, ---------------Product4
                Brand = Brand4
                Qty=40
                MRP=400.000000
, ---------------Product1
                Brand = Brand1
                Qty=10
                MRP=100.000000
, ---------------Product2
                Brand = Brand2
                Qty=15
                MRP=200.000000
]
==========================================================
Do you wish to check Out? Yes
Purchase Successful! Rs. 4600.0 debited from your account. This purchase is non refundable. Any disputes regarding this are not subjected to any jurisdiction.
Thank you for shopping with us.
PS C:\Users\SHREYANSHS\antwalkAssignments\assignment6> 
```

